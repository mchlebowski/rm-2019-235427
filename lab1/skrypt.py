import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from random import uniform
from math import sqrt

from numpy import sin, cos


def distance(q0, q1):
    return sqrt((q0[0] - q1[0]) ** 2 + (q0[1] - q1[1]) ** 2)


def Fp(q):
    return k0 * distance(q, finish_point)


def Fo(q, i, dr):
    obst_distance = max(0.25, distance(q, obst_vect[i]))
    if obst_distance > d0 or obst_distance > dr:
        return 0
    else:
        return -ko[i] * ((1 / obst_distance) - (1 / d0)) * (1 / (obst_distance ** 2))


def F(q, dr=100):
    value = 0
    value += Fp(q)
    for i in range(len(obst_vect)):
        value -= Fo(q, i, dr)
    return value


#kp = 1
delta = 0.05
x_size = 10
y_size = 10
obst_num = 30
obst_vect = []
for i in range(obst_num):
    obst_vect.append((uniform(-10, 10), uniform(-10, 10)))
start_point = (-10, uniform(-10, 10))
finish_point = (10, uniform(-10, 10))

#zad a b

k0 = 50
ko = obst_num * [10]
d0 = 5

x = y = np.arange(-10.0, 10.0, delta)
X, Y = np.meshgrid(x, y)
Z = np.exp(-X**0)

for i in range(len(x)):
    for j in range(len(y)):
        Z[i, j] = F([X[i, j], Y[i, j]])

#zad c

robot_position = [start_point]

dr = 5

while distance(robot_position[-1], finish_point) > delta * 5:
#for i in range(1000):
    F0 = np.array([0., 0.])
    force = F((robot_position[-1][0] + delta, robot_position[-1][1]), dr)
    angle = 0
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0] + delta, robot_position[-1][1] + delta), dr)
    angle = np.pi / 4
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0], robot_position[-1][1] + delta), dr)
    angle = np.pi / 2
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0] - delta, robot_position[-1][1] + delta), dr)
    angle = 3 * np.pi / 4
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0] - delta, robot_position[-1][1]), dr)
    angle = np.pi
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0] - delta, robot_position[-1][1] - delta), dr)
    angle = 5 * np.pi / 4
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0], robot_position[-1][1] - delta), dr)
    angle = 3 * np.pi / 2
    F0 += np.array([force * cos(angle), force * sin(angle)])
    force = F((robot_position[-1][0] + delta, robot_position[-1][1] - delta), dr)
    angle = 7 * np.pi / 4
    F0 += np.array([force * cos(angle), force * sin(angle)])
    F0[0] = max(min(F0[0], delta), -delta)
    F0[1] = max(min(F0[1], delta), -delta)
    forces = (-delta, delta)
    force_index = np.digitize(F0, forces)
    #print(F0)
    #print(force_index)
    newX = robot_position[-1][0] - F0[0]#forces[force_index[0]]
    newY = robot_position[-1][1] - F0[1]#forces[force_index[1]]
    #print(newX, newY)
    robot_position.append((newX, newY))

#zad d

fig = plt.figure(figsize=(10, 10))
ax = fig.add_subplot(111)
ax.set_title('Metoda potencjalow')
plt.imshow(Z, cmap=cm.RdYlGn,
           origin='lower', extent=[-x_size, x_size, -y_size, y_size])


plt.plot(start_point[0], start_point[1], "or", color='blue')
plt.plot(finish_point[0], finish_point[1], "or", color='blue')

for obstacle in obst_vect:
    plt.plot(obstacle[0], obstacle[1], "or", color='black')

for position in robot_position:
    plt.plot(position[0], position[1], "or", color='purple')

plt.colorbar(orientation='vertical')

plt.grid(True)
plt.show()
